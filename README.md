## Installing

This RMarkdown format is for rendering Rmd files to ipynb formats.

Since this format uses pandoc to convert to ipynb, at least pandoc 2.6 (which introduced converting to and from ipynb format) is necessary. 

If you do not have at least pandoc 2.6 then the format will not work.

You need to install this template directly from gitlab. First you should install the remotes package so that you can grab this package from gitlab. So in R you need to run this:

```r
install.packages("remotes")
remotes::install_gitlab("calvinw/ipynbdocument")
```

Then all you need to do to use this is put a jupyter section into the yaml of your Rmd:

### R RMarkdown 

For R use this: 

```yaml
---
title: "My R RMarkdown File"
jupyter:
  kernelspec:
    display_name: R
    language: R
    name: ir
output:
    ipynbdocument::ipynb_document
---
```

### Python RMarkdown 
For python use this:

```yaml
---
title: "My python RMarkdown File"
jupyter:
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
output:
    ipynbdocument::ipynb_document
---
```

The rest of your RMarkdown file can be whatever code you want, as long as you stick to one engine, the one in the kernelspec above. You cannot mix engines in this situation. 

The jupyter kernel you specified above is what pandoc will set the kernelspec of the ipynb to be when it creates the ipynb output.

This repo shows some simple examples of this format:

[https://gitlab.com/calvinw/rmarkdown-colab-google-docs](https://gitlab.com/calvinw/rmarkdown-colab-google-docs) 

This repo shows more complicated examples of this format:

[https://gitlab.com/calvinw/machine-learning-rmarkdown](https://gitlab.com/calvinw/machine-learning-rmarkdown) 


